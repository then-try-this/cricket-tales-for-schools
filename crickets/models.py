# Cricket Tales V2
# Copyright (C) 2018 FoAM Kernow
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from datetime import datetime
import hashlib
import time

########################################################################
# Original cricket tales modesls we share - do not touch

# for caching externally calculated values... 
class Value(models.Model):
    name = models.CharField(max_length=200)
    value = models.FloatField(default=0)
    def __unicode__(self):
        return self.name

class Player(models.Model):
    name = models.CharField(max_length=200)
    videos_watched = models.IntegerField(default=0)
    exhib = models.IntegerField(default=0)
    def __unicode__(self):
        return str(self.id)+" "+self.name

class Cricket(models.Model):
    season = models.IntegerField(default=0)
    cricket_id = models.CharField(max_length=200)
    tag = models.CharField(max_length=200)
    sex = models.CharField(max_length=200)
    activity = models.IntegerField(default=0)
    videos_ready = models.IntegerField(default=0)
    daynight_score = models.FloatField(default=0)
    def __unicode__(self):
        return self.tag+" "+self.cricket_id

class Movie(models.Model):
    cricket = models.ForeignKey(Cricket,on_delete=models.CASCADE)
    season = models.IntegerField(default=0)
    name = models.CharField(max_length=200)
    camera = models.CharField(max_length=200)
    views = models.IntegerField(default=0)
    unique_views = models.IntegerField(default=0)
    created_date = models.DateTimeField('date created')
    status = models.IntegerField(default=0)
    src_index_file = models.CharField(max_length=4096)
    start_frame = models.IntegerField(default=0)
    fps = models.FloatField(default=0)
    length_frames = models.IntegerField(default=0)
    start_time = models.DateTimeField('start time')
    end_time = models.DateTimeField('end time')
    trap_present = models.IntegerField(default=0)
    # stuff updated from periodic update.py
    num_events = models.IntegerField(default=0)
    def __unicode__(self):
        return str(self.name)+" "+str(self.camera);

class MovieView(models.Model):
    viewer = models.ForeignKey(Player,on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie,on_delete=models.CASCADE)
    
class Event(models.Model):
    movie = models.ForeignKey(Movie,on_delete=models.CASCADE)
    event_type = models.CharField(max_length=200)
    user = models.ForeignKey(Player, on_delete=models.CASCADE, null=True, blank=True, default = None)
    video_time = models.FloatField(default=0)
    estimated_real_time = models.DateTimeField(auto_now_add=True)
    actual_real_time = models.DateTimeField(null=True, blank=True, default=None)
    x_pos = models.FloatField(null=True, blank=True, default=None)
    y_pos = models.FloatField(null=True, blank=True, default=None)
    timestamp = models.DateTimeField(auto_now_add=True)
    other = models.CharField(max_length=200, null=True, blank=True, default=None)
    def __unicode__(self):
        return self.event_type+" "+str(self.video_time)+" : "+str(self.movie);


#########################################################################
# CT4schools additions
    
def _createHash():
    hash = hashlib.sha1()
    hash.update(str(time.time()).encode())
    return hash.hexdigest()[:5]
        
class ClassGroup(models.Model):
    school_name = models.CharField(max_length=200)
    class_name = models.CharField(max_length=200)
    date = models.DateTimeField()
    # hash id provides a (roughly) unique link without being able
    # to see other schools data
    group_id = models.CharField(max_length=5,default=_createHash,unique=True)
    def __str__(self):
        return self.school_name+"-"+self.class_name+"-"+self.group_id

class PlayerToClassGroup(models.Model):
    player = models.ForeignKey(Player,on_delete=models.CASCADE)    
    class_group = models.ForeignKey(ClassGroup,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.player.id)+" -> "+self.class_group.group_id

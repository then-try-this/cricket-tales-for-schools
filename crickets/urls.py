from django.urls import include, re_path

from crickets import views

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
]

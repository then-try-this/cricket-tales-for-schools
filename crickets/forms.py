import django.forms
from django.forms import ModelForm
from crickets.models import *

class EventForm(ModelForm):
     class Meta:
         model = Event
         fields = "__all__"

class LoginForm(django.forms.Form):
    group_id = django.forms.CharField(label='Enter code', max_length=5)

class ClassGroupForm(ModelForm):
    class Meta:
        model = ClassGroup
        fields = ['school_name','class_name','date','group_id']
        widgets = {
            'date': django.forms.widgets.DateTimeInput(attrs={'type': 'datetime-local'}),
            'group_id': django.forms.TextInput(attrs={'readonly':'readonly'})
        }

from django.urls import include, re_path
from django.contrib import admin
from crickets import views
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls.static import static

from django.conf import settings

urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(r'^beta/', views.index, name='index'),
    re_path(r'^exhib/', views.index_exhib, name='exhib'),
    re_path(r'^exhib_reset_lang/', views.index_exhib_reset_lang, name='exhib'),
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^about/', views.about, name='about'),
    re_path(r'^check/', views.check, name='check'),
    re_path(r'^training/', views.training, name='training'),
    re_path(r'^choose/', views.choose, name='choose'),
    re_path(r'^play/(?P<pk>\d+)/$', views.CricketView.as_view(), name='play'),
    re_path(r'^keyboard/(?P<pk>\d+)/$', views.KeyboardView.as_view(), name='keyboard'),
    re_path(r'^event/', views.record_event, name='event'),
    re_path(r'^personality/(?P<pk>\d+)/$', views.PersonalityView.as_view(), name='personality'),
    re_path(r'^results_movement/(?P<pk>\d+)/$', views.ResultsMovementView.as_view(), name='results_movement'),
    re_path(r'^results_eating/(?P<pk>\d+)/$', views.ResultsEatingView.as_view(), name='results_eating'),
    re_path(r'^results_singing/(?P<pk>\d+)/$', views.ResultsSingingView.as_view(), name='results_singing'),
    re_path(r'^player_name/', views.player_name, name='player_name'),
    re_path(r'^leaderboard/(?P<pk>\d+)/', views.LeaderboardView.as_view(), name='leaderboard'),
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    re_path(r'^login/', views.login, name='login'),
    re_path(r'^newclassgroup/', views.ClassGroupCreateView.as_view(), name='newclassgroup'),
    re_path(r'^welcome/', views.welcome, name='welcome'),
    re_path(r'^error/', views.error, name='error'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

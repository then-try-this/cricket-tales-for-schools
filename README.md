# Cricket tales for schools

A version of cricket tales to provide a teaching resource for KS3 and
higher students learning about statistics, evolution and insect
biology.
    
## Setup

Build the python environment
    
    $ python3 -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

Make the database

    $ sudo su - postgres
    $ psql
    $ create user cricket_tales_for_schools;
    $ alter user cricket_tales_for_schools with encrypted password '<insert password here>';
    $ create database cricket_tales_for_schools;
    $ grant all privileges on database cricket_tales_for_schools to cricket_tales_for_schools;

Update settings.py with this user.

Moving into django, setup the database via:
    
    $ ./manage.py migrate
    $ ./manage.py createsuperuser

This should now run the test server:
    
    $ ./manage.py runserver

